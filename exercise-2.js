"use strict";
const execute = Date.now();
function format(milliseconds) {
  let day;
  let hour;
  let minute;
  let seconds;
  seconds = Math.floor(milliseconds / 1000);
  minute = Math.floor(seconds / 60);
  seconds = seconds % 60;
  hour = Math.floor(minute / 60);
  minute = minute % 60;
  day = Math.floor(hour / 24);
  hour = hour % 24;
  console.log(
    day,
    "days",
    hour,
    "hours",
    minute,
    "minutes",
    seconds,
    "seconds"
  );
}

function timeElapsed() {
  const elapsedTime = Date.now() - execute;
  format(elapsedTime);
}
timeElapsed();

setInterval(() => {
  timeElapsed();
}, 5000);
