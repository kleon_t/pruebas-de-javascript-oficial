"use strict";

async function getUsers(number) {
  const arrayOfUsers = [];
  const url = `https://randomuser.me/api/?results=${number}`;
  const response = await fetch(url);
  const json = await response.json();
  const data = json.results;
  for (let i = 0; i < number; i++) {
    let user = {
      username: data[i].login.username,
      name: data[i].name.first,
      lastname: data[i].name.last,
      gender: data[i].gender,
      country: data[i].location.country,
      email: data[i].email,
      picture: data[i].picture.large,
    };
    arrayOfUsers.push(user);
  }
  return arrayOfUsers;
}

console.log(getUsers(5));
