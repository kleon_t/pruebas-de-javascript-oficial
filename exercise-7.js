"use strict";

let x = document.getElementById("boxCreator");

function myFunction() {
  let box = document.createElement("div");
  box.classList.add("div");
  document.getElementById("main").appendChild(box);
}
x.addEventListener("click", myFunction);
const num = () => Math.floor(Math.random() * 256);
let allTheDivs = document.getElementsByClassName("div");

setInterval(() => {
  for (let i = 0; i < allTheDivs.length; i++) {
    allTheDivs[i].style.backgroundColor = `rgb(${num()},${num()},${num()})`;
  }
}, 1000);
