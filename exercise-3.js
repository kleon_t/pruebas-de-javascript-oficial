"use strict";
function conversion(number, binaryOrDecimal) {
  if (binaryOrDecimal === 10) {
    let valor = number;
    let decimalValue = 0;
    let base = 1;

    let valorTemporal = valor;
    while (valorTemporal) {
      let last_digit = valorTemporal % 10;
      valorTemporal = Math.floor(valorTemporal / 10);

      decimalValue += last_digit * base;

      base = base * 2;
    }

    return decimalValue;
  } else if (binaryOrDecimal === 2) {
    let n = number;
    let binaryValue = new Array(32);

    let i = 0;
    while (n > 0) {
      binaryValue[i] = n % 2;
      n = Math.floor(n / 2);
      i++;
    }

    for (let j = i - 1; j >= 0; j--) document.write(binaryValue[j]);
    return binaryValue.join("");
  } else {
    console.log("Error");
  }
}
console.log(conversion(1260, 2));
